# frozen_string_literal: true
require 'csv'

class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]
  before_action :set_job_status, only: [:new, :create]

  # GET /people
  # GET /people.json
  # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def index
    @people = Person.all

    if params[:filter]
      @people = @people.haken if params[:filter].include?("haken")
      @people = @people.master_deg if params[:filter].include?("master")
      @people = @people.hired if params[:filter].include?("hired")
    end

    if params[:order]
      @people = @people.order('people.birth_date' => :desc) if params[:order] == "age_asc"
      @people = @people.order('people.birth_date' => :asc) if params[:order] == "age_desc"
    else
      @people = @people.order('people.birth_date' => :desc)
    end

    respond_to do |format|
      format.html { render :index }
      format.csv { send_data @people.to_csv, filename: "people-#{Time.zone.today}.csv" }
    end
  end
  # rubocop:enable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

  # GET /people/1
  # GET /people/1.json
  def show
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people?job_status_id=1
  # POST /people.json
  def create
    @person = Person.new(person_params)
    @person.job_statuses << @job_status

    respond_to do |format|
      if @person.save
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.js { redirect_to @person, notice: 'Person was successfully created.' }
        # format.js   { render :show, locals: {notice: 'Person was successfully created.'}}
      else
        format.html { render :new }
        format.js   { render :new }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.js   { redirect_to @person, notice: 'Person was successfully updated.' }
      else
        format.html { render :edit }
        format.js   { render :edit }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    respond_to do |format|
      if @person.destroy
        flash.notice = 'Person was successfully destroyed.'
        format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
        format.js { render inline: "location.href = document.referrer", notice: 'Person was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { render :edit }
        format.js   { render :edit }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_person
    @person = Person.find(params[:id])
  end

  def set_job_status
    @job_status = JobStatus.find(params[:job_status_id])
  end

  # Only allow a list of trusted parameters through.
  def person_params
    params.require(:person).permit(:employee_number, :name_jp, :name_en, :email,
                                   :birth_date, :joined_date, :avatar,
                                   :notes, :address, :hometown, :education, :raf, :academic_id,
                                   new_attachments: [])
  end
end
