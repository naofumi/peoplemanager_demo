# frozen_string_literal: true

class GradingsController < ApplicationController
  before_action :set_grading, only: [:show, :edit, :update, :destroy]
  before_action :set_person, only: [:new, :create]

  # GET /gradings
  # GET /gradings.json
  def index
    @gradings = Grading.includes(:person).current
    # Add sort order scope
    @gradings = case params[:grade]
                when "asc"
                  @gradings.includes(:grade).order("grades.position asc").order(started_at: :desc)
                when "desc"
                  @gradings.includes(:grade).order("grades.position desc").order(started_at: :desc)
                else
                  @gradings.order(started_at: :desc)
                end
  end

  # GET /gradings/1
  # GET /gradings/1.json
  def show
  end

  # GET /gradings/new
  def new
    @grading = Grading.new
    @grading.person = @person
  end

  # GET /gradings/1/edit
  def edit
  end

  # POST /gradings
  # POST /gradings.json
  def create
    @grading = Grading.new(grading_params)

    respond_to do |format|
      if @grading.save
        format.html { redirect_to @grading, notice: 'Grading was successfully created.' }
        format.js   { render inline: "location.reload()", notice: 'Grading was successfully created.' }
        format.json { render :show, status: :created, location: @grading }
      else
        format.html { render :new }
        format.js   { render :new }
        format.json { render json: @grading.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gradings/1
  # PATCH/PUT /gradings/1.json
  def update
    respond_to do |format|
      if @grading.update(grading_params)
        format.html { redirect_to @grading, notice: 'Grading was successfully updated.' }
        format.js   { render inline: "location.reload()", notice: 'Grading was successfully updated.' }
        format.json { render :show, status: :ok, location: @grading }
      else
        format.html { render :edit }
        format.js   { render :edit }
        format.json { render json: @grading.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gradings/1
  # DELETE /gradings/1.json
  def destroy
    @grading.destroy
    respond_to do |format|
      format.html { redirect_to gradings_url, notice: 'Grading was successfully destroyed.' }
      format.js   { render inline: "location.reload()", notice: 'Grading was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_grading
    @grading = Grading.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def grading_params
    params.require(:grading).permit(:person_id, :grade_id, :notes, :started_at, :count_start_date)
  end

  def set_person
    @person = Person.find(params[:grading][:person_id])
  end
end
