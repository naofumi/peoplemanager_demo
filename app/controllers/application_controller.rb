# frozen_string_literal: true

# Application Controller
class ApplicationController < ActionController::Base
  helper_method :display_for_date, :today

  def display_for_date
    session[:display_for_date] ||= today.to_s
    Date.strptime(session[:display_for_date])
  end

  def display_for_date=(date)
    raise "display_for_date= required a Date object as the argument" if date.class != Date

    session[:display_for_date] = date.to_s
  end

  def today
    @today ||= Time.zone.today
  end
end
