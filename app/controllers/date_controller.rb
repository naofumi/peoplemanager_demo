# DateController
#
# Manages the display-for-date
class DateController < ApplicationController

  # PATCH/PUT /date
  def update
    if params["on_date(1i)"]
      self.display_for_date = Date.new(params["on_date(1i)"].to_i,
                                       params["on_date(2i)"].to_i,
                                       params["on_date(3i)"].to_i)
    end
    respond_to do |format|
      format.html { redirect_to request.referer, notice: 'Display Date was changed' }
      format.js { render inline: "location.reload()", notice: 'Display Date was changed' }
    end
  end

end
