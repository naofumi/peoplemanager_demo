# frozen_string_literal: true

class RafsController < ApplicationController
  before_action :set_raf, only: [:show, :edit, :update, :destroy]

  # GET /rafs
  # GET /rafs.json
  def index
    @rafs = Raf
  end

  # GET /rafs/1
  # GET /rafs/1.json
  def show
  end

  # GET /rafs/new
  def new
    @raf = Raf.new
  end

  # GET /rafs/1/edit
  def edit
  end

  # POST /rafs
  # POST /rafs.json
  def create
    @raf = Raf.new(raf_params)

    respond_to do |format|
      if @raf.save
        format.html { redirect_to @raf, notice: 'Raf was successfully created.' }
        format.json { render :show, status: :created, location: @raf }
      else
        format.html { render :new }
        format.json { render json: @raf.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rafs/1
  # PATCH/PUT /rafs/1.json
  def update
    respond_to do |format|
      if @raf.update(raf_params)
        format.html { redirect_to @raf, notice: 'Raf was successfully updated.' }
        format.json { render :show, status: :ok, location: @raf }
      else
        format.html { render :edit }
        format.json { render json: @raf.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rafs/1
  # DELETE /rafs/1.json
  def destroy
    respond_to do |format|
      if @raf.destroy
        format.html { redirect_to rafs_url, notice: 'Raf was successfully destroyed.' }
        format.js { redirect_to rafs_url, notice: 'Raf was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { render :edit }
        format.js   { render :edit }
      end
    end
  end
  
  private

  # Use callbacks to share common setup or constraints between actions.
  def set_raf
    @raf = Raf.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def raf_params
    params
      .require(:raf)
      .permit(:name, :approved_date, :comments, 
              :count_requested, :number, :planning, 
              new_attachments: [])
  end
end
