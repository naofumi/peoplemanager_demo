# frozen_string_literal: true

class JobStatusesController < ApplicationController
  before_action :set_job_status, only: [:show, :edit, :update, :destroy, :remove_person]
  before_action :set_group, only: [:new]
  before_action :set_person, only: [:new]

  # GET /job_statuses
  # GET /job_statuses.json
  def index
    if params[:filter] == 'placeholders'
      @job_statuses = JobStatus.placeholders.order(started_at: :desc).all
    else
      @job_statuses = JobStatus.real.order(started_at: :desc).all
    end
  end

  # GET /job_statuses/1
  # GET /job_statuses/1.json
  def show
  end

  # GET /job_statuses/new
  def new
    @job_status = JobStatus.new
    @job_status.group = @group
    @job_status.person = @person
  end

  # GET /job_statuses/1/edit
  def edit
  end

  # POST /job_statuses
  # POST /job_statuses.json
  def create
    @job_status = JobStatus.new(job_status_params)

    respond_to do |format|
      if @job_status.save
        format.html { redirect_to @job_status, notice: 'Job status was successfully created.' }
        # format.js { redirect_to @job_status, notice: 'Job status was successfully created.' }
        format.js  { render inline: "location.reload()", notice: 'Assignment was successfully created.' }
        format.json { render :show, status: :created, location: @job_status }
      else
        format.html { render :new }
        format.js   { render :new }
        format.json { render json: @job_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_statuses/1
  # PATCH/PUT /job_statuses/1.json
  def update
    respond_to do |format|
      if @job_status.update(job_status_params)
        format.html { redirect_to @job_status, notice: 'Job status was successfully updated.' }
        format.js   { render inline: "location.reload()", notice: 'Assignment was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_status }
      else
        format.html { render :edit }
        format.js   { render :edit }
        format.json { render json: @job_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # This will remove the Person from the JobStatus association.
  # The Person will remain intact.
  def remove_person
    respond_to do |format|
      if @job_status.remove_person_and_assign_to_not_joined
        format.html { redirect_to @job_status, notice: 'Person was removed from the Job Status.' }
        format.js   { render inline: "location.reload()", notice: 'Person was removed from the Job Status.' }
        format.json { render :show, status: :ok, location: @job_status }
      else
        format.html { render :edit }
        format.js   { render :edit }
        format.json { render json: @job_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_statuses/1
  # DELETE /job_statuses/1.json
  def destroy
    @job_status.destroy
    respond_to do |format|
      format.html { redirect_to job_statuses_url, notice: 'Job status was successfully destroyed.' }
      format.js   { render inline: "location.reload()", notice: 'Assignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_job_status
    @job_status = JobStatus.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def job_status_params
    params.require(:job_status).permit(:person_id, :group_id, :grade_id, :raf_id,
                                       :started_at, :job_title_jp, :job_title_en, :reports_to_id,
                                       :group_leader, :notes, :placeholder)
  end

  def set_group
    @group = params[:job_status][:group_id] ? Group.find(params[:job_status][:group_id]) : nil
  end

  def set_person
    @person = params[:job_status][:person_id] ? Person.find(params[:job_status][:person_id]) : nil
  end
end
