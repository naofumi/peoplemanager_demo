module PeopleHelper
  def css_class_for(person)
    if person.hired
      ""
    else
      "open"
    end
  end

  def image_tag_for_no_photo(args = {})
    image_tag("faces/no-photo.png", args)
  end

  def image_tag_for_person(person, args = {})
    # if person.avatar.attached?
    if false
      url = url_for(person.avatar)
      link_to image_tag(url, args), url
    else
      image_tag_for_no_photo(args)
    end
  end

  # filter_link "派遣", :haken
  def filter_link_to(title, filter_tag)
    params_array = params[:filter].to_a
    filter_tag_as_string = filter_tag.to_s
    if params_array.include?(filter_tag_as_string)
      new_params_array = params_array.uniq - [filter_tag_as_string]
      css_class = "btn btn-primary"
    else
      new_params_array = (params_array + [filter_tag_as_string]).uniq
      css_class = "btn btn-outline-primary"
    end
    link_to title, people_path(modified_params_hash(filter: new_params_array)), class: css_class
  end

  def order_link_to(title, order_tag)
    order_tag_as_string = order_tag.to_s
    css_class = if params[:order] == order_tag_as_string
                  "btn btn-primary"
                else
                  "btn btn-outline-primary"
                end
    link_to title,
            people_path(modified_params_hash(order: order_tag_as_string)),
            class: css_class
  end

  def time_since_joined_in_words(person)
    if person.joined_date && (person.joined_date <= Time.zone.today)
      distance_of_time_in_words_to_now person.joined_date
    else
      ""
    end
  end
end
