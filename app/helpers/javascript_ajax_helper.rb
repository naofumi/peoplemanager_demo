module JavascriptAjaxHelper

  # Generate JavaScript to replace the innerHTML of
  # the DOM element (identified by `:id`) with `:content`.
  #
  # It can also set the innerHTML of the `#notice` DOM element
  # using the option notice: "[notice content]".
  # Arguments:
  # id: The id specifing the DOM element into which `content` will
  #     be inserted using `innerHTML`.
  # content: The content to be inserted.
  # Options:
  # notice: Set the innerHTML of `#notice`. If not specified,
  #         `#notice` innerHTML will be reset.
  # textareas_to_adjust: Specify an array of textarea DOM IDs.
  #         The height of each element will be set to that of
  #         the content after insertion, with the minimum height
  #         at 50px.
  def js_insert_content_into_id(id:, content:, **options)
    notice = options[:notice]
    result = <<~SOCJS
      document.getElementById('#{id}').innerHTML = "#{j(content)}";
      document.getElementById('notice').innerHTML = "#{j(notice)}";
    SOCJS
    if taa = options[:textareas_to_adjust]
      result += <<~TAJS
        for (i of #{taa.to_json}){
          var pn = document.getElementById(i);
          var scroll_height = pn.scrollHeight;
          var target_height = Math.max(scroll_height, "50");
          pn.style.height = target_height + \"px\";
        };
      TAJS
    end
    result.html_safe
  end

end
