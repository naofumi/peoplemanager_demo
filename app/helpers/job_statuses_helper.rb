# frozen_string_literal: true

# JobStatusesHelper
module JobStatusesHelper
  def link_to_job_status(job_status)
    if (person = job_status.person)
      class_string = person.joined? ? '' : 'yet_to_join'
      tag.span link_to("#{person.name_jp}-#{job_status.group.name_jp}",
                       edit_job_status_path(job_status),
                       {remote: true, data: {toggle:"modal", target:"#detailsModal"}}),
               class: class_string
    else
      tag.span link_to( "OPEN: #{job_status.job_title_jp} #{job_status.group.name_jp}",
                        edit_job_status_path(job_status),
                        {remote: true, data: {toggle:"modal", target:"#detailsModal"}}),
               class: 'yet_to_join'
    end
  end

  def link_to_job_status_in_org_chart(job_status)
    label = "OPEN "
    if job_status.raf
      label += "RAF #{job_status.raf.planning} #{!job_status.raf.approved_date.blank? ? "Approved" : "To be approved"}"
    else
      label += "no RAF"
    end
    link_to label, edit_job_status_path(job_status),
            {remote: true, data: {toggle:"modal", target:"#detailsModal"}}
  end


end
