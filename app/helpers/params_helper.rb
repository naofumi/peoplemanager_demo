# Provide helpers for managing `params`
# and other parameter manipulations
module ParamsHelper
  # Modify the parameters in `params` with 
  # `params_hash_modifications` hash
  # and return a new hash that can be used 
  # for url parameter generation.
  #
  # If `params` has {"filter" => ["a", "b"]}, then
  # modified_params_hash(filter: ["c", "d"]) will return
  # {"filter" => ["c", "d"]}
  def modified_params_hash(params_hash_modifications)
    params_hash = params.except(:controller, :action).to_unsafe_hash
    params_hash_modifications.each_key do |key|
      params_hash[key.to_s] = params_hash_modifications[key]
    end
    params_hash
  end
end
