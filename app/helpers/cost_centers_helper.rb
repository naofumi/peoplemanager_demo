module CostCentersHelper
  def cost_center_headcount_by_group
    @head_count_by_cost_center ||= CostCenter.headcount_by_cost_center(display_for_date)
  end
end
