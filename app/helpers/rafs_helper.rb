module RafsHelper
  def raf_number_and_name(raf)
    "#{raf_number raf} #{raf.name}"
  end

  def raf_number(raf)
    format "#%04<number>d", number: raf.number
  end

  def raf_select_options
    [
      ["unspecified", 
       Raf.all_unspecified.map { |r| [raf_number_and_name(r), r.id] }], 
      ["specified", 
       Raf.all_specified.map { |r| [raf_number_and_name(r), r.id] }]
    ]
  end
end
