# frozen_string_literal: true

# Application Helper
module ApplicationHelper
  MARKDOWN_RENDERER = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)

  def markdown
    MARKDOWN_RENDERER
  end

  def markdown_syntax_link
    link_to 'Markdown記法', 'http://www.markdown.jp/syntax/', { target: '_blank', rel: 'noopener'}
  end

  def markdown_render(md_string)
    # Markdown render result should be OK to html_safe
    markdown.render(md_string.to_s).html_safe # rubocop:disable Rails/OutputSafety(
  end

  # Default error messages for object
  # To be used on edit forms
  def error_messages_box(object)
    if object.errors.any?
      <<~ERROR_MESSAGES_BOX.html_safe
      <div id="error_explanation">
        <h2>#{pluralize(object.errors.count, "error")} prohibited this #{object.model_name.singular} from being saved:</h2>

        <ul>
          #{object.errors.full_messages.map{ |message| tag.li message }.join("\n")}
        </ul>
      </div>
      ERROR_MESSAGES_BOX
    end
  end
end
