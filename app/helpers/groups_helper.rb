# frozen_string_literal: true

# GroupsHelper
module GroupsHelper
  def groups_for_select
    nested_set_options Group do |i|
      "#{'  ' * i.level}|-#{i.name_jp}"
    end
  end

  def russian_doll_tree(group, date, &block)
    result = "<div class=\"tree tree-depth-#{group.depth}\">".html_safe # rubocop:disable Rails/OutputSafety
    result += capture(group, date, &block)
    group.children.each do |child|
      result += russian_doll_tree(child, date, &block)
    end
    result += '</div>'.html_safe
    result
  end
end
