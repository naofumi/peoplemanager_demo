module BootstrapHelper

  def bs_scope(bs_scope_options, &block)
    options_stash = @bs_scope_options
    @bs_scope_options = bs_scope_options
    yield if block_given?
    @bs_scope_options = options_stash
  end

  def bs_label_width
    (@bs_scope_options && @bs_scope_options[:form_label_width]) || 4
  end

  def bs_nav_item(name = nil, path = nil)
    link_class = request.fullpath.include?(path) ? "nav-link active" : "nav-link"
    link_to name, path, class: link_class
  end

  def bs_text_field(form = nil, name = nil, text = nil)
    tag.div(class: "form-group row") do
      form.label(name, text, class: "col-sm-#{bs_label_width}") +
      tag.div(class: "col-sm-#{12 - bs_label_width}") do
        form.text_field name, class: "form-control"
      end
    end
  end

  def bs_text_area(form = nil, name = nil, text = nil)
    tag.div(class: "form-group") do
      form.label(name, text) +
      form.text_area(name, class: "form-control")
    end
  end

  def bs_select(form, name, text = nil, choices = nil, options = {}, html_options = {}, &block)
    html_options[:class] = [html_options[:class], "form-control"].compact.join(' ')
    tag.div(class: "form-group row") do
      form.label(name, text, class: "col-sm-#{bs_label_width}") +
      tag.div(class: "col-sm-#{12 - bs_label_width}") do
        form.select name, choices, options, html_options, &block
      end
    end
  end

  def bs_checkbox(form, name, text = nil)
    tag.div(class: "form-group row") do
      form.label(name, text, class: "col-sm-#{bs_label_width}") +
      tag.div(class: "col-sm-#{12 - bs_label_width}") do
        form.check_box name, class: "form-control"
      end
    end
  end

  # Display label, attribute value pairs in a Object#show view
  # in a Bootstrap Grid with the label in the first column
  # and the value in the second column.
  #
  # If a block is given, then the output of the block will form
  # the content of the second column.
  #
  # If surrounded by a `bs_scope`, then the value provided as
  # `form_label_width:` will determine the width of the label column.
  def bs_attribute label, value = nil
    tag.div(class: "row my-1") do
      tag.div(label + ":", class: "font-weight-bold col-md-#{bs_label_width}") +
      if block_given?
        tag.div(class: "col-md-#{12 - bs_label_width}") do
          capture do
            yield
          end
        end
      else
        tag.div(value, class: "col-md-#{12 - bs_label_width}")
      end
    end
  end

  # Create a help button, which, when clicked, will show a modal box
  # with the contents of the block.
  #
  # The modal will be generated into a the named yield `:help` which
  # will be rendered in `layout/application`.
  #
  # Usage
  # <% bs_help_modal "Modal title" do %>
  #   <dl>
  #     <dt>About ***</dt>
  #     <dd>Details of ***</dd>
  #   </dl>
  # <% end %>
  def bs_help_modal title, &block
    yield_out = capture do
      yield
    end
    help_content = <<~BS_MODAL.html_safe
      <button type="button" class="float-right btn btn-link" data-toggle="modal" data-target="#help-modal">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-question-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
          <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
        </svg>
      </button>

      <!-- Modal -->
      <div class="modal fade" id="help-modal" tabindex="-1" aria-labelledby="help-modalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="help-modalLabel">#{title}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              #{yield_out}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <!--         <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
          </div>
        </div>
      </div>
    BS_MODAL
    content_for :help, help_content
  end

  def bs_modal_header(title:)
    <<~BS_MODAL_HEADER.html_safe
      <div class="modal-header">
        <h5 class="modal-title">#{title}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    BS_MODAL_HEADER
  end

  # Convinience method to simplify HTML generation for BootStrap modals
  # Allows for modification of modal size.
  #
  # Usage
  #
  # <% bs_details_modal _arguments_ do %>
  #     ... some HTML ...
  # <% end %>
  #
  # Argument size: can be the BootStrap modal size classes, "modal-sm", "modal-lg" or "modal-xl".
  # Additionally, size: can be "none" in which case the modal will fill the screen.
  def bs_details_modal(size:"")
    style_string = ""
    size_class = ""
    if ["modal-sm", "modal-lg", "modal-xl"].include? size
      size_class = size
    elsif size == "none"
      style_string = "max-width: none !important; margin: 0.5rem"
    end
    <<~BS_DETAILS_MODAL.html_safe
      <div class="modal-dialog #{size_class}" style="#{style_string}">
        <div class="modal-content">
          #{capture { yield }}
        </div>
      </div>
    BS_DETAILS_MODAL
  end

end
