class Grade < ApplicationRecord
  SUPER_GRADES = ['Haken', 'Contract', 'S', 'C', 'E', 'J'].freeze
  acts_as_list
  has_many  :gradings, dependent: :restrict_with_error
  has_many  :people, through: :gradings, dependent: :restrict_with_error

  validates :name, presence: true
  validates :super_grade, presence: true
  validates :super_grade, inclusion: {in: SUPER_GRADES, message: "%<value>s は許可されていない値です"}

  # Counts the super_grades for all people belonging to specified groups
  # at the specified date.
  # Returns something like {'Haken' => 2, 'S' => 5, 'E' => 1, 'J' => 1}
  def self.head_count_grouped_by_grades_for_groups_at_date(groups, date)
    all_job_statuses = JobStatus.at(date)
                                .where(group_id: groups.collect(&:id))
    grouped_grades = Grading.at(date).joins(:grade)
                            .where(person_id: all_job_statuses.real.collect(&:person_id))
                            .group(:super_grade).count
    Grade::SUPER_GRADES.each { |super_grade| grouped_grades[super_grade] ||= 0 }
    grouped_grades['not-Graded'] = all_job_statuses.real.left_outer_joins(person: :gradings)
                                  .where("gradings.id IS NULL").count
    grouped_grades['Open'] = all_job_statuses.placeholders.count
    grouped_grades
  end

end
