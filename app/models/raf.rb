# frozen_string_literal: true

# Discussion on how we should handle RAFs
# ===================
# 1. RAFs should be join RAFs and JobStatuses
#    This should be obvious since we OPEN JobStatuses will usually not
#    be connected to Person objects.
# 2. RAFs should be have certain statuses.
#      a. Pre-approval status
#      b. Approved status with comments and an approved date
#         This could be triggered by simply entering the date
#      c. Pre-hiring status that indicates that not all hires are finished
#      d. Positions filled status that indicates that all positions have
#         been filled with high-probability candidates, but not yet joined.
#         This could be inferred by the availability of a non-placeholder
#         JobStatus on the associated Person object
#      e. Hiring finished status that indicates that the candidates have all
#         joined.
#         This could be inferred by whether the current JobStatus of each
#         associated Person is non-placeholder or not.
#      If a high-probability candidate is identified, then we should assign
#      a Person object to the placeholder JobStatus and add a non-placeholder JobStatus to
#      that Person with a starting date.
#      If that candidate chooses not to join, then we should delete that Person
#      while leaving the placeholder JobStatus intact (which will maintain the
#      connection to the RAF)
#   The workflow for creating RAF and associating them with JobStatus should
#   be as follows.
#   1. You first create a placeholder JobStatus for the Position you will
#      hire.
#   2. You also create a RAF for this.
#   3. From the JobStatus, associate to this RAF.
class Raf < ApplicationRecord
  has_many  :job_statuses, dependent: :restrict_with_error

  validates :count_requested, numericality: {greater_than_or_equal_to: 1}
  validates :number, uniqueness: true
  validates :planning, presence: true

  # ActiveStorage
  # Read https://evilmartians.com/chronicles/rails-5-2-active-storage-and-beyond
  # for more information on ActiveStorage
  has_many_attached :attachments

  before_save :auto_assign_number_if_nil

  scope :active, -> { where(active: true) }

  def job_statuses_unspecified?
    job_statuses.size < count_requested
  end

  def joining_completed?
    number_who_joined >= count_requested
  end

  def number_who_joined
    job_statuses.filter { |js| js.person&.hired }.size
  end

  def self.all_unspecified
    Raf.all.filter(&:job_statuses_unspecified?)
  end

  def self.all_specified
    Raf.all.reject(&:job_statuses_unspecified?)
  end

  def status
    if approved_date.blank?
      'Pending approval'
    elsif approved_date && !joining_completed?
      'Approved and Hiring'
    elsif approved_date && joining_completed?
      'Hiring finished'
    end
  end

  # Use the uploaded new_attachment params to 
  # add attachments to what we already have
  def new_attachments=(params)
    attachments.attach(params)
  end

  private

  def auto_assign_number_if_nil
    self.number ||= Raf.maximum(:number) + 1
  end
end
