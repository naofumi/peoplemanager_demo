module DatesCommon
  extend ActiveSupport::Concern

  included do
    # callbacks
  end

  private

  def years_since(date, from_date = Time.zone.today)
    if date && from_date
      result = from_date.year - date.year
      result -= 1 if from_date < date + result.years
    else
      result = nil
    end
    result    
  end

  def date_difference_in_years(date, from_date = Time.zone.today)
    (from_date - date).to_i / 365.0 if date && from_date
  end
end
