class TrainingCategory < ApplicationRecord
  has_many :training_programs, dependent: :restrict_with_error
end
