class TrainingProgram < ApplicationRecord
  belongs_to :training_category
  has_many :enrollments, dependent: :restrict_with_error

  validates :training_category_id, presence: true
  validates :name, presence: true

  # Convert price strings like "¥4,000" to decimal numbers
  def price=(value)
    self[:price] = value.blank? ? nil : value.gsub(/[^0-9.]/, '').to_d
  end
end
