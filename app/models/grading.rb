# frozen_string_literal: true

# Gradings assign Grades to People
class Grading < ApplicationRecord
  include DatesCommon

  belongs_to  :grade
  belongs_to  :person

  validates :started_at, presence: true
  validates :count_start_date, presence: true

  after_save :assign_ended_at_for_all_gradings_of_person

  # Arbitrary very distant future date used
  # to set JobStatus#ended_at if this is the latest
  # JobStatus for a Person.
  DISTANT_FUTURE_DATE = Date.new(3000, 1, 1)

  # Current gradings
  scope :current, -> { where('gradings.started_at <= ? AND gradings.ended_at >= ?', Time.zone.today, Time.zone.today) }
  # Gradings at date
  scope :at, ->(date) { where('gradings.started_at <= ? AND gradings.ended_at >= ?', date, date) }
  # Haken
  scope :haken, -> { joins(:grade).where('grades.super_grade' => 'Haken') }

  # During first year will be 1 year in grade.
  # This is not the real years in grade but for grading purposes
  def years_in_grade
    end_date = Time.zone.today > ended_at ? ended_at : Time.zone.today
    date_difference_in_years(count_start_date, end_date)
  end

  private

  def assign_ended_at_for_all_gradings_of_person
    gradings_of_person = person.gradings.order(started_at: :desc)
    ended_at = DISTANT_FUTURE_DATE
    gradings_of_person.each do |g|
      # Updating of the :ended_at column should not chnage updated_at
      g.update_column(:ended_at, ended_at - 1.day) # rubocop:disable Rails/SkipsModelValidations
    end
  end
end
