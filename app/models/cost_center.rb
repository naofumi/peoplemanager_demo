require 'csv'
# CostCenters are associated with Groups
#
# A Group may specifiy a cost_center_id which will
# specify cost_centers of decendant groups.
#
# For every group, if the group specifies a cost_center_id
# of if an ancestor specifies a cost_center_id, then the
# cost_center of that group will be the cost_center specified
# by that ID.
# If there are several ancestors with differing cost_center_ids,
# then the ancestor that is closest to the current group will
# specify the cost_center of the group.
class CostCenter < ApplicationRecord
  validates :number, presence: true
  validates :number, uniqueness: true
  validates :description, presence: true

  has_many  :groups, dependent: :restrict_with_error

  def direct_and_indirect_groups
    result = []
    Group.all.each do |g|
      cc = g.derived_cost_center
      if cc == self
        result << g
      end
    end
    result
  end

  def self.headcount_by_cost_center(date)
    result = {}
    Group.all.each do |g|
      cc = g.derived_cost_center
      if !cc.nil?
        result[cc] ||= {}
        result[cc].merge!(g.count_people_by_super_grade_at(date)) {|key, v1, v2|
          v1 + v2
        }
      else #cc is nil
        result[cc] ||= []
        result[cc] << g
      end
    end
    result
  end

  def self.to_csv(date)
    CSV.generate(headers: true) do |csv|
      csv << ["cost_center_id", "cost_center_description", "group", "super_grade", "name_jp", "name_en", "started_at"]
      Group.all.each do |g|
        cc = g.derived_cost_center
        if !cc.nil?
          g.job_statuses.real.at(date).each do |js|
            # All 'real' (non-placeholder) JobStatuses must have a Person
            p = js.person
            if gd = p.gradings.at(date).first
              # person with grade
              csv << [cc.number, cc.description, g.name_jp, gd.grade.super_grade, p.name_jp, p.name_en, js.started_at]
            else
              # person who is not-graded
              csv << [cc.number, cc.description, g.name_jp, 'not-graded', p.name_jp, p.name_en, js.started_at]
            end
          end
          g.job_statuses.placeholders.each do |js|
            # open position
            csv << [cc.number, cc.description, g.name_jp, 'OPEN', js.job_title_jp, js.job_title_en, js.started_at]
          end
        else
          # Ignore if no CostCenter associated
        end
      end
    end
  end

end
