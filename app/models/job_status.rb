# frozen_string_literal: true

# What is a JobStatus a.k.a. Assignment
#
# A JobStatus associates a Person to a Group. It tells you that a Person is part of a Group during a certain
# period of time. It sounds similar to a Job Position but is very distinct. Whereas a Job Position is
# concerned mostly about the title and where it fits in the organizaiton chart, a JobStatus is always associated
# with a Person and is more concerned with telling you what jobs a person experienced and when. We are using
# the JobStatus object to give the necessary functionality that you would expect from a JobPosition object,
# but still being Person focused. In particular, the focus in on how we handle OPEN positions.
#
# When you create an open position, you create a JobStatus with placeholder: true. This is associated with
# the Group that the open position belongs to. It will show up and be counted as an OPEN position.
# Normally, a JobStatus must be associated with a Person.
# However, as long as the placeholder is true, then this is not necessary. Indeed, JobStatuses for OPEN
# positions should initially be created without an associated Person. You can add notes to the JobStatus
# or add information in the title part to provide more information about the JobStatus.
#
# If you find a candidate that is likely to join, then you could add a Person object to the JobStatus.
# The placeholder attribute should still be set as true since this is not yet a true JobStatus - the Person
# isn't actually doing the job yet. This will give you an idea of which positions are likely to be filled
# in the near future with whom as you gaze at the current Org Chart.
# Sometimes the candidate decides to go elsewhere and does not join AGC.
# In this case, we don't need to keep a history so it is best to remove the Person and to preserve the
# JobStatus since that position will still be OPEN.
#
# When the hiring date is fixed, then you can create a new JobStatus with the placeholder attribute set to false
# that connects the Person to the Group. You set the start date to the starting date of that hire. With this
# done, when the date comes, that Person will show up in the Org Chart in that position. Thereafter, this
# Person is treated as normal.
#
# This Person will then have two JobStatuses. One is the placeholder from before he was hired, and the second
# is for after he was hired. The former placeholder is still valuable since that JobStatus will be associated with
# the RAF and therefore we can confirm whether a RAF was filled or not. However, the function of that JobStatus
# will be very different from a regular JobStatus, so we display it in a different way.
#
#
# To manage this complexity, we should be careful to set validations and restrictions on JobStatuses.
# 1. JobStatuses cannot be orphaned and must be associated with a Person and Group, unless they are placeholders.
# 2. JobStatuses should not be deleted unless there is an error. A JobStatus is history and must be preserved.
# 3. Likewise, JobStatuses should not be changed when the Person switches roles.
#    Instead, a new JobStatus should be made.
#    It is OK to change a JobStatus if there has been an error or additional information is needed.
# 3. People should not be deleted unless they were never hired to begin with. That is,
#    any Person with an associated non-placeholder
#    JobStatus cannot be deleted. In the rare case that you created a non-placeholder with a start_date in the future
#    in anticipation of that Person being hired at a specific date, but that Person didn't join,
#    then you will first have to
#    delete that non-placeholder JobStatus before you can delete that Person.
# 4. Do not delete a placeholder: true JobStatus for the purpose of deleting a Person object. It is unnecessary
#    and will also screw up RAFs. JobStatuses with associated RAFs should not be deleted.
# 5. We need to change how RAFs work. Currently, they are associated to People and not JobStatuses. This has to change
#    if we want JobStatus to work in the way that we just described.
class JobStatus < ApplicationRecord
  belongs_to :person, optional: true
  belongs_to :group
  belongs_to :reports_to, class_name: 'Person', optional: true
  belongs_to :raf, optional: true

  validates :started_at, presence: true
  validates :ended_at, presence: true
  validates :person_id, presence: true, unless: :placeholder

  before_validation :set_ended_at_if_nil
  after_save :set_ended_at_for_all_job_statuses_of_person, if: :person_id

  # Arbitrary very distant future date used
  # to set JobStatus#ended_at if this is the latest
  # JobStatus for a Person.
  DISTANT_FUTURE_DATE = Date.new(3000, 1, 1)

  # Current JobStatuses
  scope :current, lambda {
    where('job_statuses.started_at <= ? AND job_statuses.ended_at >= ?', Time.zone.today, Time.zone.today)
  }
  # JobStatuses at date
  scope :at, ->(date) { where('job_statuses.started_at <= ? AND job_statuses.ended_at >= ?', date, date) }
  # Not a placeholder
  scope :real, -> { where('job_statuses.placeholder' => false) }
  # Placeholders
  scope :placeholders, -> { where('job_statuses.placeholder' => true) }
  # Leaders
  scope :leaders, -> { where('job_statuses.group_leader' => true) }

  def remove_person_and_assign_to_not_joined
    JobStatus.transaction do
      p = person
      update!(person: nil)
      p.job_statuses.create!(group_id: Group.not_joined_group.id, started_at: Time.zone.today)
    rescue ActiveRecord::RecordInvalid
      errors.add(:base, "Job Status failed invalidations")
    end
  end

  # Previous non-placeholder JobStatus
  def previous
    return nil if placeholder
    if person
      jss = person.job_statuses.real.order(started_at: :asc).to_a
      i = jss.index(self)
      if i > 0
        jss[i - 1]
      else
        nil
      end
    else
      nil
    end
  end

  # Previous non-placeholder JobStatus
  def next
    return nil if placeholder
    if person
      jss = person.job_statuses.real.order(started_at: :asc).to_a
      i = jss.index(self)
      if i + 1 < jss.size
        jss[i + 1]
      else
        nil
      end
    else
      nil
    end
  end

  private

  def set_ended_at_if_nil
    self.ended_at = DISTANT_FUTURE_DATE if ended_at.blank?
  end

  def set_ended_at_for_all_job_statuses_of_person
    job_statuses_of_person = person.job_statuses.order(started_at: :desc)
    ended_at = DISTANT_FUTURE_DATE
    job_statuses_of_person.each do |js|
      # updating of :ended_at should not update :updated_at
      js.update_column(:ended_at, ended_at - 1.day) # rubocop:disable Rails/SkipsModelValidations
      ended_at = js.started_at
    end
  end
end
