class Enrollment < ApplicationRecord
  belongs_to  :person
  belongs_to  :training_program

  validates :person, presence: true
  validates :training_program, presence: true
  validates :due_date, presence: true

  scope :finished, -> { where(["finished_date < ?", Time.zone.today]) }
  scope :unfinished, -> { where(["finished_date is null OR finished_date >= ?", Time.zone.today]) }
end
