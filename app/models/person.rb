# frozen_string_literal: true

# Person lifecycle
#
# We don't want orphaned Person objects lying around so we need to clearly define how People are created and
# how they can be destroyed.
#
# Since we don't want orphan People lying around, a Person must always be associated with a JobStatus.
# Therefore, we won't create a Person independently, but instead always create a JobStatus and then
# create a Person. A Person always needs to have an assoicated job_status.
class Person < ApplicationRecord
  include DatesCommon

  ACADEMIC = ['High School', 'College', 'Bachelor', 'Master', 'Doctor'].freeze
  # JobStatus dependency is set to :nullify. However,
  # we will separately verify in a before_destroy callback
  # that there are no JobStatus objects with placeholder: false.
  # To ensure this before_destroy is called before :nullify, place it
  # before `has_many  :job_statuses, dependent: :nullify`, since
  # dependent: :nullify also registers into the before_destroy callback
  # chain.
  before_destroy :ensure_absence_of_non_placeholder_job_statuses

  # ActiveStorage
  # Read https://evilmartians.com/chronicles/rails-5-2-active-storage-and-beyond
  # for more information on ActiveStorage
  has_one_attached :avatar
  has_many_attached :attachments

  has_many  :job_statuses, dependent: :nullify
  has_many  :gradings, dependent: :restrict_with_error
  has_many  :enrollments, dependent: :restrict_with_error
  delegate :job_title_jp, :job_title_en, :group, :group_leader, to: :current_status, allow_nil: true
  delegate :grade, to: :current_grading, allow_nil: true
  validates :name_jp, presence: true
  validates :name_en, format: { with: /\A\w+,\s\w+\z/, message: ' is not valid En name. Use [Family name], [Given name] format.', allow_blank: true }
  validates :job_statuses, presence: true
  validates :email, format: { with: /\A[\w\-.]+@agc(bio)?\.com/, message: 'is not a valid AGC/AGC Bio email address' },
                    allow_blank: true

  # Currently hired employees
  scope :hired, lambda {
    joins(:job_statuses)
      .where("job_statuses.started_at < ? AND ? < job_statuses.ended_at", Time.zone.today, Time.zone.today)
      .where("job_statuses.placeholder" => false)
      .where("job_statuses.group_id" => Group.hired_groups.map(&:id))
  }

  # Current People as placeholders for open positions
  scope :open, lambda {
    joins(:job_statuses)
      .where("job_statuses.started_at < ? AND ? < job_statuses.ended_at",
             Time.zone.today,
             Time.zone.today)
      .where("job_statuses.placeholder" => true)
  }

  # Current orphaned People (with no associated current_status)
  scope :orphaned, lambda {
    joins('LEFT OUTER JOIN job_statuses ON job_statuses.person_id = people.id').where('job_statuses.id IS NULL')
  }
  # Has Master's degree
  scope :master_deg, -> { where(academic_id: Person::ACADEMIC.find_index('Master')) }
  # Haken
  scope :haken, -> { where(id: Grading.haken.current.collect(&:person_id)) }

  def academic
    ACADEMIC[academic_id] if academic_id.present?
  end

  def age(date = Time.zone.today)
    years_since(birth_date, date)
  end

  def years_since_joined
    years_since(joined_date)
  end

  def joined_at_age
    years_since(birth_date, joined_date)
  end

  def current_status
    @current_status ||= job_statuses.current.first
  end

  def current_grading
    @current_grading ||= gradings.current.first
  end

  def joined?
    job_statuses.current.real.any?
  end

  def hired
    current_status && !current_status.placeholder ? true : false
  end

  def grade_at(date)
    grading = gradings.at(date).first
    grading ? grading.grade : nil
  end

  def haken_at?(date)
    grade = grade_at(date)
    grade && (grade.name == 'Haken') ? true : false
  end

  # Clean up name
  def name_jp=(string)
    string.sub!(/(?u)\s/, ' ') # (?u) turns on Unicode matching
    self[:name_jp] = string
  end

  def given_name_en
    splitted_name_en.first
  end

  def family_name_en
    splitted_name_en.last
  end

  def splitted_name_en
    return [] if name_en.blank?
    @splitted_name_en ||= name_en.split(%r{(?u),\s*})
  end

  # Get an array of managers in which this Person reports to.
  # Each Group returns leaders as an Array of People since
  # there can be multiple Group leaders in the current implementation
  def reporting_line
    @reporting_line ||= group.self_and_ancestors.reverse.map do |g|
      gl = g.current_leaders
      gl.empty? ? nil : gl
    end.compact!
  end

  def direct_report
    reporting_line.first
  end

  # This is for generating a CSV based report
  # where the reporting line should be padded
  # with the highest in hierarchy at the end
  # and empty ranks as nil.
  # The number of elements is defined as array_size
  def reporting_line_padded(array_size = 6)
    (Array.new(array_size, []) + reporting_line)[-array_size..-1]
  end

  # Use the uploaded new_attachment params to
  # add attachments to what we already have
  def new_attachments=(params)
    attachments.attach(params)
  end

  # Acts on a scope
  def self.to_csv
    CSV.generate(headers: true) do |csv|
      all.each do |p|
        row = []
        row << p.given_name_en
        row << p.family_name_en
        row << p.name_jp
        row << p.email
        row << p.birth_date
        row << "CHB"
        row << p.joined_date
        row << (p.haken_at?(Time.zone.today) ? "Haken" : "FTE")
        row << ""
        row << ""
        row << ""
        row << p.years_since_joined
        row << p.group.name_jp
        row << p.reporting_line.first&.first&.given_name_en
        row << p.reporting_line.first&.first&.family_name_en
        p.reporting_line_padded(6).each do |r|
          if r.first.nil?
            row << ""
            row << ""
          else
            row << r.first.name_en
            row << r.first.group.name_jp
          end
        end
        csv << row
      end
    end
  end

  # obfuscators for demo
  def name_jp
    "山田 太郎"
  end

  def name_en
    "Jon Doe"
  end

  def notes
    "何かメモ"
  end

  def email
    "some-email@somedomain.com"
  end

  def employee_number
    123456
  end

  def birth_date
    Date.new(1980,1,1)
  end

  def joined_date
    Date.new(2010,2,1)
  end

  private

  def ensure_absence_of_non_placeholder_job_statuses
    return if job_statuses.real.empty?

    errors.add :job_statuses, 'which are not placeholders remain.'
    throw :abort
  end
end
