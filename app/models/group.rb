# frozen_string_literal: true

# Represents a group within an organization.
# Typically, it will define a set of JobStatuses for
# the same job function. This makes it easier to understand
# open positions. Hence it will often be smaller than an
# organizational group.
class Group < ApplicationRecord
  HIRED_BY_GROUP_NAME = "Japan AGC Biologics Related"
  DID_NOT_JOIN_GROUP_NAME = "Did not join"

  acts_as_nested_set counter_cache: :children_count
  has_many :job_statuses, dependent: :restrict_with_error
  has_many :people, through: :job_statuses

  has_many :current_leader_job_statuses, -> { leaders.current }, class_name: "JobStatus"
  has_many :current_leaders, through: :current_leader_job_statuses, source: :person

  belongs_to  :cost_center, optional: true

  def self.recursive_loop(nodes, &block)
    raise ArgumentError, 'Missing block' unless block_given?

    nodes.each do |node|
      block.call(node)
      node.children.each do |child|
        recursive_loop([child], &block)
      end
    end
  end

  # This is a group to put people who made it half-way through the hiring process,
  # had a Person object assigned (because the probability of joining was deemed high),
  # but eventually did not join.
  def self.not_joined_group
    Group.default_scoped.find_or_create_by!(name_jp: DID_NOT_JOIN_GROUP_NAME) do |group|
      group.notes = "Progressed in hiring but did not join"
    end
  end

  # Groups that contain people who are currently hired
  def self.hired_groups
    Group.find_by!(name_jp: HIRED_BY_GROUP_NAME).self_and_descendants
  end

  # Sort job_statuses by leader status and the associated person's grade and birth_date
  def job_statuses_sorted_by_grade_at(date)
    non_placeholder_js = job_statuses.at(date)
                                     .includes(:person).joins(person: { gradings: :grade })
                                     .where('gradings.started_at <= ? AND ? <= gradings.ended_at',
                                            Time.zone.today, Time.zone.today)
                                     .order(group_leader: :desc, 'grades.position' => :asc, 'people.birth_date' => :asc)

    non_placeholder_non_graded_js = job_statuses.at(date)
                                     .includes(:person).left_outer_joins(person: :gradings)
                                     .where('gradings.id IS NULL')
                                     .order(placeholder: :asc, group_leader: :desc, 'people.birth_date' => :asc)

    # placeholder_js = job_statuses.at(date)
    #                              .includes(:person).where(placeholder: true)

    non_placeholder_js + non_placeholder_non_graded_js
  end

  # Counts the super_grades for all people belonging to this
  # group (does not include descending groups).
  # Returns something like {'Haken' => 2, 'S' => 5, 'E' => 1, 'J' => 1}
  def count_people_by_super_grade_at(date)
    Grade.head_count_grouped_by_grades_for_groups_at_date([self], date)
  end

  # Counts the super_grades for all people belonging to this
  # group and all descending groups.
  # Returns something like {'Haken' => 2, 'S' => 5, 'E' => 1, 'J' => 1}
  def self_and_descendents_count_people_by_super_grade_at(date)
    Grade.head_count_grouped_by_grades_for_groups_at_date(self_and_descendants, date)
  end

  # Cost_center of the closest ancestor with a cost_center_id specified
  def derived_cost_center
    self_and_ancestors.where("cost_center_id IS NOT NULL").last&.cost_center
  end

end
