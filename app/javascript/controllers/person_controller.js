// Visit The Stimulus Handbook for more details 
// https://stimulusjs.org/handbook/introduction
// 
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>

import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "toggleNotesButton", "notesText" ]

  connect() {
  	// console.log("Stimulus connected", this.element);
  }

  toggleNotes(event) {
  	const button = this.toggleNotesButtonTarget
  	const notesText = this.notesTextTarget
  	if (notesText.style.display == "none") {
  		notesText.style.display = 'block'
  		button.innerText = "Hide Notes"
  	} else {
  		notesText.style.display = 'none';
  		button.innerText = "Notes are Hidden: Show notes?";
  	}
  	event.preventDefault()
  }
}
