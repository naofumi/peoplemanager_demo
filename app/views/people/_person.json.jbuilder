json.extract! person, :id, :name_jp, :name_en, :email, :birth_date, :joined_date, :notes, :created_at, :updated_at
json.url person_url(person, format: :json)
