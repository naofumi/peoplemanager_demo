json.extract! job_status, :id, :person_id, :group_id, :grade_id, :started_at, :job_title_jp, :job_title_en, :reports_to_id, :created_at, :updated_at
json.url job_status_url(job_status, format: :json)
