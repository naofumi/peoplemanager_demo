json.extract! raf, :id, :name, :approved_date, :comments, :created_at, :updated_at
json.url raf_url(raf, format: :json)
