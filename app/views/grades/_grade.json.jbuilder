json.extract! grade, :id, :name, :position, :created_at, :updated_at
json.url grade_url(grade, format: :json)
