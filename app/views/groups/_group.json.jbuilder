json.extract! group, :id, :parent_id, :name_jp, :name_en, :position, :created_at, :updated_at
json.url group_url(group, format: :json)
