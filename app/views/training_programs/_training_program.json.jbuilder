json.extract! training_program, :id, :code, :name, :price, :url, :description, :created_at, :updated_at
json.url training_program_url(training_program, format: :json)
