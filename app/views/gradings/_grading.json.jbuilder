json.extract! grading, :id, :person_id, :grade_id, :notes, :started_at, :created_at, :updated_at
json.url grading_url(grading, format: :json)
