json.extract! training_category, :id, :name, :description, :created_at, :updated_at
json.url training_category_url(training_category, format: :json)
