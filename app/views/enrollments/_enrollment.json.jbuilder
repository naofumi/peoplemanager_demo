json.extract! enrollment, :id, :person_id, :training_program_id, :due_date, :finished_date, :comment, :created_at, :updated_at
json.url enrollment_url(enrollment, format: :json)
