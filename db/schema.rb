# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_30_053556) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "cost_centers", force: :cascade do |t|
    t.integer "number", null: false
    t.string "description", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["number"], name: "index_cost_centers_on_number", unique: true
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer "person_id"
    t.integer "training_program_id"
    t.date "due_date"
    t.date "finished_date"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "grades", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "notes"
    t.string "super_grade"
  end

  create_table "gradings", force: :cascade do |t|
    t.integer "person_id", null: false
    t.integer "grade_id", null: false
    t.text "notes"
    t.date "started_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "ended_at"
    t.date "count_start_date", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.integer "parent_id"
    t.string "name_jp"
    t.string "name_en"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "lft", default: 0, null: false
    t.integer "rgt", default: 0, null: false
    t.integer "depth"
    t.integer "children_count"
    t.text "notes"
    t.integer "cost_center_id"
    t.index ["lft"], name: "index_groups_on_lft"
    t.index ["parent_id"], name: "index_groups_on_parent_id"
    t.index ["rgt"], name: "index_groups_on_rgt"
  end

  create_table "job_statuses", force: :cascade do |t|
    t.integer "person_id"
    t.integer "group_id", null: false
    t.date "started_at", null: false
    t.string "job_title_jp"
    t.string "job_title_en"
    t.integer "reports_to_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "notes"
    t.date "ended_at", null: false
    t.boolean "group_leader", default: false
    t.boolean "placeholder", default: false, null: false
    t.integer "raf_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "name_jp"
    t.string "name_en"
    t.string "email"
    t.date "birth_date"
    t.date "joined_date"
    t.text "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "employee_number"
    t.text "address"
    t.string "hometown"
    t.text "education"
    t.string "raf"
    t.integer "academic_id"
  end

  create_table "rafs", force: :cascade do |t|
    t.string "name"
    t.date "approved_date"
    t.text "comments"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "count_requested", null: false
    t.integer "number", null: false
    t.string "planning", null: false
    t.index ["number"], name: "index_rafs_on_number", unique: true
  end

  create_table "training_categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "training_programs", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.decimal "price"
    t.string "url"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "training_category_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "enrollments", "people"
  add_foreign_key "enrollments", "training_programs"
  add_foreign_key "gradings", "grades"
  add_foreign_key "gradings", "people"
  add_foreign_key "job_statuses", "groups"
  add_foreign_key "job_statuses", "people"
  add_foreign_key "job_statuses", "rafs"
  add_foreign_key "training_programs", "training_categories"
end
