class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.integer :parent_id
      t.string :name_jp
      t.string :name_en
      t.integer :order

      t.timestamps
    end
  end
end
