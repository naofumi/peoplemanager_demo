class AddSuperGradeToGrades < ActiveRecord::Migration[6.0]
  def change
    add_column  :grades, :super_grade, :string
  end
end
