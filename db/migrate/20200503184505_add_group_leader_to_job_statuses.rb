class AddGroupLeaderToJobStatuses < ActiveRecord::Migration[6.0]
  def change
    add_column  :job_statuses, :group_leader, :boolean, default: false
  end
end
