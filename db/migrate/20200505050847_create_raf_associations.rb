class CreateRafAssociations < ActiveRecord::Migration[6.0]
  def change
    create_table :raf_associations do |t|
      t.integer :person_id
      t.integer :raf_id
      t.text :comments

      t.timestamps
    end
  end
end
