class DropRafAssociations < ActiveRecord::Migration[6.0]
  def change
    drop_table  :raf_associations
  end
end
