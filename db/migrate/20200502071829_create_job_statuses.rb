class CreateJobStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :job_statuses do |t|
      t.integer :person_id
      t.integer :group_id
      t.integer :grade_id
      t.date :started_at
      t.string :job_title_jp
      t.string :job_title_en
      t.integer :reports_to_id

      t.timestamps
    end
  end
end
