class AddAddressToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people, :address, :text
  end
end
