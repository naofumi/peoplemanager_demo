class AddNotNullForJobStatuses < ActiveRecord::Migration[6.0]
  def change
    change_column :job_statuses, :started_at, :date, null: false
    change_column :job_statuses, :ended_at, :date, null: false
  end
end
