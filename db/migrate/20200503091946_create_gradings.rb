class CreateGradings < ActiveRecord::Migration[6.0]
  def change
    create_table :gradings do |t|
      t.integer :person_id
      t.integer :grade_id
      t.text :notes
      t.date :started_at

      t.timestamps
    end
  end
end
