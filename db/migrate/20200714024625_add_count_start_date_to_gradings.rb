class AddCountStartDateToGradings < ActiveRecord::Migration[6.0]
  def up
    add_column  :gradings, :count_start_date, :date, :null => true
    Grading.all.each do |g|
      if g.started_at
        g.update_columns(count_start_date: g.started_at)
      else
        g.update_columns(count_start_date: Date.today)
      end        
    end
    change_column :gradings, :count_start_date, :date, null: false
  end

  def down
    remove_column  :gradings, :count_start_date, :date, :null => false
  end
end
