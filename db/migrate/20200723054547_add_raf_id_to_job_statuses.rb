class AddRafIdToJobStatuses < ActiveRecord::Migration[6.0]
  def change
    add_column  :job_statuses, :raf_id, :integer, null: true
    add_foreign_key :job_statuses, :rafs
  end
end
