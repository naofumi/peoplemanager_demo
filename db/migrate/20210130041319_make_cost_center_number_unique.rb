class MakeCostCenterNumberUnique < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :groups, :cost_centers
    add_index :cost_centers, :number, unique: true
  end
end
