class CreateRafs < ActiveRecord::Migration[6.0]
  def change
    create_table :rafs do |t|
      t.string :name
      t.date :approved_date
      t.text :comments

      t.timestamps
    end
  end
end
