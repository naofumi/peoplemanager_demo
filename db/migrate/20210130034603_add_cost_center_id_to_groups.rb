class AddCostCenterIdToGroups < ActiveRecord::Migration[6.0]
  def change
    add_column  :groups, :cost_center_id, :integer
  end
end
