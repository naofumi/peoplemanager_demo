class RemoveForeignKeyForGroupsCostCenters < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :groups, :cost_centers
  end
end
