class AddActiveToRaf < ActiveRecord::Migration[6.0]
  def change
    add_column  :rafs, :active, :boolean
  end
end
