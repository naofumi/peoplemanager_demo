class FixColumnNames < ActiveRecord::Migration[6.0]
  def change
    rename_column :groups, :order, :position
    rename_column :grades, :order, :position
  end
end
