class AddEndedAtToJobStatusesAndGradings < ActiveRecord::Migration[6.0]
  def change
    add_column :job_statuses, :ended_at, :date
    add_column :gradings, :ended_at, :date
  end
end
