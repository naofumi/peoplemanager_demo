class CreateEnrollments < ActiveRecord::Migration[6.0]
  def change
    create_table :enrollments do |t|
      t.integer :person_id
      t.integer :training_program_id
      t.date :due_date
      t.date :finished_date
      t.text :comment

      t.timestamps
    end
  end
end
