class AddPlanningToRafs < ActiveRecord::Migration[6.0]
  def up
    add_column :rafs, :planning, :string
    Raf.all.each do |r|
      r.update_column :planning, "PLAN"
    end
    change_column :rafs, :planning, :string, null: false
  end

  def down
    remove_column :rafs, :planning, :string
  end
end
