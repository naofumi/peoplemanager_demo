class DeletePositionFromGroups < ActiveRecord::Migration[6.0]
  def change
    remove_column :groups, :position, :integer
  end
end
