class AddForeignKeysToTables < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :enrollments, :people, index: true
    add_foreign_key :enrollments, :training_programs, index: true

    add_foreign_key :gradings, :people, index: true
    add_foreign_key :gradings, :grades, index: true

    add_foreign_key :job_statuses, :people, index: true
    add_foreign_key :job_statuses, :groups, index: true

    add_foreign_key :raf_associations, :people, index: true
    add_foreign_key :raf_associations, :rafs, index: true
  end
end
