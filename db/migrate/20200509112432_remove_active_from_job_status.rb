class RemoveActiveFromJobStatus < ActiveRecord::Migration[6.0]
  def change
    remove_column :job_statuses, :active, :boolean
  end
end
