class CreatePeople < ActiveRecord::Migration[6.0]
  def change
    create_table :people do |t|
      t.string :name_jp
      t.string :name_en
      t.string :email
      t.date :birth_date
      t.date :joined_date
      t.string :photo_url
      t.text :notes

      t.timestamps
    end
    add_index :people, :email, unique: true
  end
end
