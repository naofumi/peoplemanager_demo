class CreateTrainingCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :training_categories do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
