class AddCountToRafs < ActiveRecord::Migration[6.0]
  def change
    add_column  :rafs, :count_requested, :integer
  end
end
