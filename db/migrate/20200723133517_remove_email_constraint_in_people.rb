class RemoveEmailConstraintInPeople < ActiveRecord::Migration[6.0]
  def up
    remove_index  :people, name: :index_people_on_email
  end

  def down
    add_index :people, :email, unique: true
  end
end
