class CreateTrainingPrograms < ActiveRecord::Migration[6.0]
  def change
    create_table :training_programs do |t|
      t.string :code
      t.string :name
      t.decimal :price
      t.string :url
      t.text :description

      t.timestamps
    end
  end
end
