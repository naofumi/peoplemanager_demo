class AddTrainingProgramCategoryToTrainingPrograms < ActiveRecord::Migration[6.0]
  def change
    add_column  :training_programs, :training_category_id, :integer
  end
end
