class AddMissingForeignKeyRestrictions < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key "training_programs", "training_categories"
  end
end
