class AddEducationToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column  :people, :education, :text
  end
end
