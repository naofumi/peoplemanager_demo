class RemoveHiredFromPeople < ActiveRecord::Migration[6.0]
  def change
    remove_column :people, :hired, default: true
  end
end
