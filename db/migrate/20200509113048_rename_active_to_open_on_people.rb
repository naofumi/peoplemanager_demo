class RenameActiveToOpenOnPeople < ActiveRecord::Migration[6.0]
  def change
    rename_column :people, :active, :open
  end
end
