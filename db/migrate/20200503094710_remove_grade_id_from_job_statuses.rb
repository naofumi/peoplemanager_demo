class RemoveGradeIdFromJobStatuses < ActiveRecord::Migration[6.0]
  def change
    remove_column :job_statuses, :grade_id, :integer
  end
end
