class AddNotesToTables < ActiveRecord::Migration[6.0]
  def change
    add_column :groups, :notes, :text
    add_column :job_statuses, :notes, :text
    add_column :grades, :notes, :text
  end
end
