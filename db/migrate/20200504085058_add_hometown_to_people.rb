class AddHometownToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column :people, :hometown, :string
  end
end
