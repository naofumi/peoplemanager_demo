class CreateCostCenters < ActiveRecord::Migration[6.0]
  def change
    create_table :cost_centers do |t|
      t.integer :number, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
