class RemoveActiveFromRafs < ActiveRecord::Migration[6.0]
  def change
    remove_column :rafs, :active, :boolean
  end
end
