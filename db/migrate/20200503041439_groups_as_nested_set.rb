class GroupsAsNestedSet < ActiveRecord::Migration[6.0]
  def self.up
    # Category.where(parent_id: 0).update_all(parent_id: nil) # Uncomment this line if your project already has :parent_id
    add_column :groups, :lft, :integer, null: false, default: 0
    add_column :groups, :rgt, :integer, null: false, default: 0
    change_column_null :groups, :parent_id, true

    # optional fields
    add_column :groups, :depth,          :integer
    add_column :groups, :children_count, :integer

    #indexes
    add_index  :groups, :lft
    add_index  :groups, :rgt
    add_index  :groups, :parent_id


    # This is necessary to update :lft and :rgt columns
    Group.reset_column_information
    Group.rebuild!
  end

  def self.down
    remove_column :categories, :lft
    remove_column :categories, :rgt

    # optional fields
    remove_column :categories, :depth
    remove_column :categories, :children_count
  end
end
