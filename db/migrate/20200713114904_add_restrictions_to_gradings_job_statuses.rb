class AddRestrictionsToGradingsJobStatuses < ActiveRecord::Migration[6.0]
  def change
    change_column :job_statuses, :person_id, :integer, null: false
    change_column :job_statuses, :group_id, :integer, null: false
    change_column :gradings, :person_id, :integer, null: false
    change_column :gradings, :grade_id, :integer, null: false
  end
end
