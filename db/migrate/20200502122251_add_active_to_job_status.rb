class AddActiveToJobStatus < ActiveRecord::Migration[6.0]
  def change
  	add_column	:job_statuses, :active, :boolean
  end
end
