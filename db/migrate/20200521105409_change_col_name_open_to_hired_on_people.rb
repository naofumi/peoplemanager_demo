class ChangeColNameOpenToHiredOnPeople < ActiveRecord::Migration[6.0]
  def change
    rename_column :people, :open, :hired
  end
end
