class AddAcademicIdToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column  :people, :academic_id, :integer
  end
end
