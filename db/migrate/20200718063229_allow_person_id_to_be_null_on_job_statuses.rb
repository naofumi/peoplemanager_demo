class AllowPersonIdToBeNullOnJobStatuses < ActiveRecord::Migration[6.0]
  def change
    change_column :job_statuses, :person_id, :integer, null: true
  end
end
