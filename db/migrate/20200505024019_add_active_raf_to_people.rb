class AddActiveRafToPeople < ActiveRecord::Migration[6.0]
  def change
    add_column  :people, :active, :boolean, default: true
    add_column  :people, :raf, :string
  end
end
