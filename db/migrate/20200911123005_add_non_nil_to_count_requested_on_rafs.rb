class AddNonNilToCountRequestedOnRafs < ActiveRecord::Migration[6.0]
  def change
    change_column :rafs, :count_requested, :integer, null: false
  end
end
