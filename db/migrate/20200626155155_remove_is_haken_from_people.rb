class RemoveIsHakenFromPeople < ActiveRecord::Migration[6.0]
  def change
    remove_column :people, :is_haken, :boolean
  end
end
