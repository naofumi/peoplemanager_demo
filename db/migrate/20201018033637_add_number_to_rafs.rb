class AddNumberToRafs < ActiveRecord::Migration[6.0]
  def up
    add_column :rafs, :number, :integer
    Raf.order(:id).each.with_index(1) do |r, i|
      r.update_column :number, i
    end
    add_index :rafs, :number, unique: true
    change_column :rafs, :number, :intger, null: false
  end

  def down
    remove_column :rafs, :number, :integer
  end
end
