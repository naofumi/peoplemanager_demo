class AddPlaceholderToJobStatuses < ActiveRecord::Migration[6.0]
  def change
    add_column  :job_statuses, :placeholder, :boolean, default: false, null: false
    JobStatus.all.each do |js|
      js.update_attributes! placeholder: false
    end
  end
end
