module AuthenticationHelper
  def basic_auth_header
    user = 'boo'
    pw = 'hoo'
    {'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Basic.encode_credentials(user, pw)}
  end
end 
