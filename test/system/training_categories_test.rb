require "application_system_test_case"

class TrainingCategoriesTest < ApplicationSystemTestCase
  setup do
    @training_category = training_categories(:one)
  end

  test "visiting the index" do
    visit training_categories_url
    assert_selector "h1", text: "Training Categories"
  end

  test "creating a Training category" do
    visit training_categories_url
    click_on "New Training Category"

    fill_in "Description", with: @training_category.description
    fill_in "Name", with: @training_category.name
    click_on "Create Training category"

    assert_text "Training category was successfully created"
    click_on "Back"
  end

  test "updating a Training category" do
    visit training_categories_url
    click_on "Edit", match: :first

    fill_in "Description", with: @training_category.description
    fill_in "Name", with: @training_category.name
    click_on "Update Training category"

    assert_text "Training category was successfully updated"
    click_on "Back"
  end

  test "destroying a Training category" do
    visit training_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Training category was successfully destroyed"
  end
end
