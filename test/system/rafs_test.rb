require "application_system_test_case"

class RafsTest < ApplicationSystemTestCase
  setup do
    @raf = rafs(:one)
  end

  test "visiting the index" do
    visit rafs_url
    assert_selector "h1", text: "Rafs"
  end

  test "creating a Raf" do
    visit rafs_url
    click_on "New Raf"

    fill_in "Approved date", with: @raf.approved_date
    fill_in "Comments", with: @raf.comments
    fill_in "Name", with: @raf.name
    click_on "Create Raf"

    assert_text "Raf was successfully created"
    click_on "Back"
  end

  test "updating a Raf" do
    visit rafs_url
    click_on "Edit", match: :first

    fill_in "Approved date", with: @raf.approved_date
    fill_in "Comments", with: @raf.comments
    fill_in "Name", with: @raf.name
    click_on "Update Raf"

    assert_text "Raf was successfully updated"
    click_on "Back"
  end

  test "destroying a Raf" do
    visit rafs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Raf was successfully destroyed"
  end
end
