require "application_system_test_case"

class JobStatusesTest < ApplicationSystemTestCase
  setup do
    @job_status = job_statuses(:one)
  end

  test "visiting the index" do
    visit job_statuses_url
    assert_selector "h1", text: "Job Statuses"
  end

  test "creating a Job status" do
    visit job_statuses_url
    click_on "New Job Status"

    fill_in "Grade", with: @job_status.grade_id
    fill_in "Group", with: @job_status.group_id
    fill_in "Job title en", with: @job_status.job_title_en
    fill_in "Job title jp", with: @job_status.job_title_jp
    fill_in "Person", with: @job_status.person_id
    fill_in "Reports to", with: @job_status.reports_to_id
    fill_in "Started at", with: @job_status.started_at
    click_on "Create Job status"

    assert_text "Job status was successfully created"
    click_on "Back"
  end

  test "updating a Job status" do
    visit job_statuses_url
    click_on "Edit", match: :first

    fill_in "Grade", with: @job_status.grade_id
    fill_in "Group", with: @job_status.group_id
    fill_in "Job title en", with: @job_status.job_title_en
    fill_in "Job title jp", with: @job_status.job_title_jp
    fill_in "Person", with: @job_status.person_id
    fill_in "Reports to", with: @job_status.reports_to_id
    fill_in "Started at", with: @job_status.started_at
    click_on "Update Job status"

    assert_text "Job status was successfully updated"
    click_on "Back"
  end

  test "destroying a Job status" do
    visit job_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Job status was successfully destroyed"
  end
end
