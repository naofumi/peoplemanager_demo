require 'test_helper'

class CostCenterTest < ActiveSupport::TestCase

  test "number is unique" do
    cc = cost_centers(:one)
    ncc = CostCenter.new(number: cc, description: "CC with duplicate number")
    assert_not ncc.save
  end
end
