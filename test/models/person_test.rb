require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "age on birthday" do
    person = people(:born_1990_6_1)
    assert_equal person.age(Date.new(2020, 6, 1)), 30
  end

  test "age before birthday" do
    person = people(:born_1990_6_1)
    assert_equal person.age(Date.new(2020, 2, 1)), 29
  end

  test "age after birthday" do
    person = people(:born_1990_6_1)
    assert_equal person.age(Date.new(2020, 8, 1)), 30
  end

  test "Person without JobStatus will not validate" do
    person = people(:without_associations)
    assert_not person.save
    assert_not_empty person.errors.messages[:job_statuses]
  end

  test "Person name_jp will be cleaned up on assignment" do
    person = people(:one)
    person.name_jp = "加々美　直史"
    assert_equal person.name_jp, "加々美 直史"
  end

  test "validate email for AGC or AGC Biologics" do
    person = people(:one)
    person.email = "naofumi@gmail.com"
    assert_not person.save
    assert_not_empty person.errors.messages[:email]

    person.email = "naofumi@agc.com"
    assert person.save

    person.email = "naofumi@agcbio.com"
    assert person.save
  end

  test "can delete person with only placeholder job_statuses" do
    person = people(:with_only_placeholder_job_statuses)
    assert_difference("Person.count", -1) do
      person.destroy
    end
  end

  test "cannot delete person with both placeholder and non-placeholder job_statuses" do
    person = people(:with_placeholder_job_statuses_and_non_placeholder_job_status)
    assert_difference("Person.count", 0) do
      person.destroy
    end
    assert_not_empty person.errors[:job_statuses]
  end

  test "test format of name_en which should be 'Familyname, Givenname'" do
    p = people(:one)
    p.name_en = "Naofumi Kagami"
    assert_not p.save
    assert_not_empty p.errors.messages[:name_en]
  end

  test "name_en can be blank" do
    p = people(:one)
    p.name_en = nil
    assert p.save
  end

  test "direct report" do
    p = people(:one)
    assert p.direct_report.first == people(:two)
  end
end
