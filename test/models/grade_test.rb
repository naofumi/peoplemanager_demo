require 'test_helper'

class GradeTest < ActiveSupport::TestCase
  test "should not allow invalid SuperGrade values" do
    g = Grade.new(name: "new grade", notes: "my string", super_grade: "ZZZZZZ")
    assert_not g.save
  end
end
