require 'test_helper'

class JobStatusTest < ActiveSupport::TestCase
  test "Allow creation of JobStatus with report_to set as nil" do
    new_job_status = new_job_status_from_template
    new_job_status.reports_to_id = nil
    new_job_status.save

    assert_empty new_job_status.errors.messages
  end

  test "Job Status must have Person if not placeholder" do
    new_job_status = new_job_status_from_template
    new_job_status.person_id = nil

    assert_not new_job_status.save, "job status without person_id should fail validations"
  end

  test "Job Status doesn't need Person if placeholder" do
    new_job_status = new_job_status_from_template
    new_job_status.placeholder = true
    new_job_status.person_id = nil

    assert new_job_status.save, "job status can be saved without person_id if placeholder: true"
  end

  test "Remove Person association from JobStatus if #placeholder = false" do
    js = job_statuses(:placeholder_job_status_one)
    js.update(person: nil)
    js.reload

    assert_nil js.person
  end

  test "remove_person_and_assign_to_not_joined if #placeholder = true" do
    js = job_statuses(:placeholder_job_status_one)
    p = js.person
    js.remove_person_and_assign_to_not_joined
    js.reload

    assert_nil js.person
    assert_includes Group.not_joined_group.people, p
  end

  test "fail to remove_person_and_assign_to_not_joined if #placeholder = false" do
    js = job_statuses(:non_placeholder_job_status_two)
    p = js.person
    js.remove_person_and_assign_to_not_joined
    js.reload

    assert_not_nil js.person
    assert_not_includes Group.not_joined_group.people, p
  end

  private

  def new_job_status_from_template(label = :one)
    template_job_status = job_statuses(label)
    JobStatus.new(started_at: template_job_status.started_at,
                  person_id: template_job_status.person_id,
                  group_id: template_job_status.group_id,
                  group_leader: template_job_status.group_leader,
                  job_title_en: template_job_status.job_title_en,
                  job_title_jp: template_job_status.job_title_jp,
                  reports_to_id: template_job_status.reports_to_id,
                  notes: template_job_status.notes)
  end
end
