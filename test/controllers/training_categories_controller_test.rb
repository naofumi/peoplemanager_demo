require 'test_helper'
require 'helpers/authentication_helper'

class TrainingCategoriesControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @training_category = training_categories(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get training_categories_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_training_category_url, headers: @default_headers
    assert_response :success
  end

  test "should create training_category" do
    assert_difference('TrainingCategory.count') do
      post training_categories_url, 
           params: { training_category: { description: @training_category.description, 
                                          name: @training_category.name } },
           headers: @default_headers
    end

    assert_redirected_to training_category_url(TrainingCategory.last)
  end

  test "should show training_category" do
    get training_category_url(@training_category), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_training_category_url(@training_category), headers: @default_headers
    assert_response :success
  end

  test "should update training_category" do
    patch training_category_url(@training_category), 
          params: { training_category: { description: @training_category.description, 
                                         name: @training_category.name } },
          headers: @default_headers
    assert_redirected_to training_category_url(@training_category)
  end

  test "should fail to destroy training_category with associations" do
    assert_difference('TrainingCategory.count', 0) do
      delete training_category_url(@training_category), headers: @default_headers
    end

    # show :edit with errors
    assert_response :success
  end
end
