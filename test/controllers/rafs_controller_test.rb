require 'test_helper'
require 'helpers/authentication_helper'

class RafsControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @raf = rafs(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get rafs_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_raf_url, headers: @default_headers
    assert_response :success
  end

  test "should create raf" do
    assert_difference('Raf.count') do
      post rafs_url, 
           params: { raf: { approved_date: @raf.approved_date, 
                            comments: @raf.comments, 
                            name: @raf.name,
                            planning: "PLAN",
                            count_requested: 1 } },
           headers: @default_headers
    end

    assert_redirected_to raf_url(Raf.last)
  end

  test "should show raf" do
    get raf_url(@raf), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_raf_url(@raf), headers: @default_headers
    assert_response :success
  end

  test "should update raf" do
    patch raf_url(@raf), 
          params: { raf: { approved_date: @raf.approved_date, 
                           comments: @raf.comments, 
                           name: @raf.name } },
          headers: @default_headers
    assert_redirected_to raf_url(@raf)
  end
end
