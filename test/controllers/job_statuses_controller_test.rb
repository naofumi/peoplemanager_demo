require 'test_helper'
require 'helpers/authentication_helper'

class JobStatusesControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper

  setup do
    @job_status = job_statuses(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get job_statuses_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_job_status_url(job_status: {group_id: groups(:one).id }), headers: @default_headers
    assert_response :success
  end

  test "should fail to get new without person_id" do
    assert_raise do
      get new_job_status_url, headers: @default_headers
      assert_response :success
    end
  end

  test "should create job_status" do
    assert_difference('JobStatus.count') do
      post  job_statuses_url, 
            params: { job_status: {group_id: @job_status.group_id, 
                                   job_title_en: @job_status.job_title_en, 
                                   job_title_jp: @job_status.job_title_jp, 
                                   person_id: @job_status.person_id, 
                                   reports_to_id: @job_status.reports_to_id, 
                                   started_at: @job_status.started_at, 
                                   notes: @job_status.notes, 
                                   ended_at: @job_status.ended_at, 
                                   group_leader: @job_status.group_leader } }, 
            headers: @default_headers
    
    end

    assert_redirected_to job_status_url(JobStatus.last)
  end

  test "should show job_status" do
    get job_status_url(@job_status), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_job_status_url(@job_status), headers: @default_headers
    assert_response :success
  end

  test "should update job_status" do
    patch job_status_url(@job_status), 
          params: { job_status: { group_id: @job_status.group_id, 
                                  job_title_en: @job_status.job_title_en, 
                                  job_title_jp: @job_status.job_title_jp, 
                                  person_id: @job_status.person_id, 
                                  reports_to_id: @job_status.reports_to_id, 
                                  started_at: @job_status.started_at, 
                                  ended_at: @job_status.ended_at, 
                                  group_leader: @job_status.group_leader, 
                                  notes: @job_status.notes } },
          headers: @default_headers
    assert_redirected_to job_status_url(@job_status)
  end

  test "should destroy job_status" do
    assert_difference('JobStatus.count', -1) do
      delete job_status_url(@job_status), headers: @default_headers
    end

    assert_redirected_to job_statuses_url
  end
  
  test "should remove associated person if placeholder job_status" do
    js = job_statuses(:placeholder_job_status_one)
    assert_not_nil js.person

    put remove_person_job_status_url(js), headers: @default_headers

    js.reload
    assert_nil js.person
  end

  test "should fail to remove associated person if non-placeholder job_status" do
    js = job_statuses(:non_placeholder_job_status_two)
    assert_not_nil js.person

    put remove_person_job_status_url(js), headers: @default_headers

    js.reload
    assert_not_nil js.person
  end
end
