require 'test_helper'
require 'helpers/authentication_helper'

class GroupsControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper

  setup do
    @group = groups(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get groups_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_group_url, headers: @default_headers
    assert_response :success
  end

  test "should create group" do
    assert_difference('Group.count') do
      post  groups_url,
            headers: @default_headers,
            params: { group: { name_en: @group.name_en,
                               name_jp: @group.name_jp,
                               parent_id: @group.parent_id,
                               cost_center_id: @group.cost_center_id,
                               notes: @group.notes } }
    end

    assert_redirected_to group_url(Group.last)
  end

  test "should show group" do
    get group_url(@group), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_group_url(@group), headers: @default_headers
    assert_response :success
  end

  test "should update group" do
    patch group_url(@group),
          headers: @default_headers,
          params: { group: { name_en: @group.name_en,
                             name_jp: @group.name_jp,
                             parent_id: @group.parent_id,
                             notes: @group.notes,
                             cost_center_id: @group.cost_center_id } }
    assert_redirected_to group_url(@group)
  end

  test "should fail to destroy group with associations" do
    assert_difference('Group.count', 0) do
      delete group_url(@group), headers: @default_headers
    end

    # show :edit with errors
    assert_response :success
  end
end
