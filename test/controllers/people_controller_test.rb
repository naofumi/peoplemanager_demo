require 'test_helper'
require 'helpers/authentication_helper'

class PeopleControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @person = people(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']    
  end

  test "should get index" do
    get people_url, headers: @default_headers
    assert_response :success
  end

  test "should get haken index" do
    get people_url(filter: ["haken"]), headers: @default_headers
    assert_response :success
  end

  test "should get master index" do
    get people_url(filter: ["master"]), headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_person_url(job_status_id: job_statuses(:one).id), headers: @default_headers
    assert_response :success
  end

  test "should create person with job_status_id" do
    job_status = job_statuses(:wo_person)
    assert_difference('Person.count') do
      post people_url, params: { job_status_id: job_status.id, 
                                 person: { birth_date: @person.birth_date, 
                                           email: 'uniquemail@agc.com', 
                                           joined_date: @person.joined_date, 
                                           name_en: @person.name_en, 
                                           name_jp: @person.name_jp, 
                                           notes: @person.notes } },
                       headers: @default_headers
    end
    assert_redirected_to person_url(Person.last)
  end

  test "should show person" do
    get person_url(@person), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_person_url(@person), headers: @default_headers
    assert_response :success
  end

  test "should update person" do
    patch person_url(@person), 
          params: { person: { birth_date: @person.birth_date, 
                              email: @person.email, 
                              joined_date: @person.joined_date, 
                              name_en: @person.name_en, 
                              name_jp: @person.name_jp, 
                              notes: @person.notes } },
          headers: @default_headers
    assert_redirected_to person_url(@person)
  end

  test "should destroy person" do
    assert_difference('Person.count', -1) do
      delete person_url(people(:without_associations)), headers: @default_headers
    end

    assert_redirected_to people_url
  end
end
