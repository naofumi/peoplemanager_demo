require 'test_helper'
require 'helpers/authentication_helper'

class GradesControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @grade = grades(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get grades_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_grade_url, headers: @default_headers
    assert_response :success
  end

  test "should create grade" do
    assert_difference('Grade.count') do
      post grades_url, 
           params: { grade: { name: @grade.name, 
                              super_grade: @grade.super_grade, 
                              notes: @grade.notes } },
           headers: @default_headers
    end

    assert_redirected_to grade_url(Grade.last)
  end

  test "should show grade" do
    get grade_url(@grade), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_grade_url(@grade), headers: @default_headers
    assert_response :success
  end

  test "should update grade" do
    patch grade_url(@grade), 
          params: { grade: { name: @grade.name, 
                             super_grade: @grade.super_grade, 
                             notes: @grade.notes } },
          headers: @default_headers
    assert_redirected_to grade_url(@grade)
  end

  test "should fail to destroy grade with associations" do
    assert_difference('Grade.count', 0) do
      delete grade_url(@grade), headers: @default_headers
    end

    assert_redirected_to grades_url
  end
end
