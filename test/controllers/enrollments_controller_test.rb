require 'test_helper'
require 'helpers/authentication_helper'

class EnrollmentsControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @enrollment = enrollments(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get enrollments_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_enrollment_url, 
        params: {'enrollment[person_id]' => people(:one).id}, 
        headers: @default_headers
    assert_response :success
  end

  test "should create enrollment" do
    assert_difference('Enrollment.count') do
      post  enrollments_url,
            headers: @default_headers,
            params: { enrollment: { comment: @enrollment.comment, 
                                    due_date: @enrollment.due_date, 
                                    finished_date: @enrollment.finished_date, 
                                    person_id: @enrollment.person_id, 
                                    training_program_id: @enrollment.training_program_id } }
    end

    assert_redirected_to enrollment_url(Enrollment.last)
  end

  test "should show enrollment" do
    get enrollment_url(@enrollment), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_enrollment_url(@enrollment), headers: @default_headers
    assert_response :success
  end

  test "should update enrollment" do
    patch enrollment_url(@enrollment),
          headers: @default_headers,
          params: { enrollment: { comment: @enrollment.comment, 
                                  due_date: @enrollment.due_date, 
                                  finished_date: @enrollment.finished_date, 
                                  person_id: @enrollment.person_id, 
                                  training_program_id: @enrollment.training_program_id } }
    assert_redirected_to enrollment_url(@enrollment)
  end

  test "should destroy enrollment" do
    assert_difference('Enrollment.count', -1) do
      delete enrollment_url(@enrollment), headers: @default_headers
    end

    assert_redirected_to enrollments_url
  end
end
