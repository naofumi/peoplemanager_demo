require 'test_helper'
require 'helpers/authentication_helper'

class TrainingProgramsControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @training_program = training_programs(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get training_programs_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_training_program_url, headers: @default_headers
    assert_response :success
  end

  test "should create training_program" do
    # assert_difference('TrainingProgram.count') do
    post  training_programs_url, 
          params: { training_program: { code: @training_program.code, 
                                        description: @training_program.description, 
                                        name: @training_program.name, 
                                        price: @training_program.price, 
                                        url: @training_program.url, 
                                        training_category_id: training_categories(:one).id } },
          headers: @default_headers
    # end

    assert_redirected_to training_program_url(TrainingProgram.last)
  end

  test "should show training_program" do
    get training_program_url(@training_program), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_training_program_url(@training_program), headers: @default_headers
    assert_response :success
  end

  test "should update training_program" do
    patch training_program_url(@training_program), 
          params: { training_program: { code: @training_program.code, 
                                        description: @training_program.description, 
                                        name: @training_program.name, 
                                        price: @training_program.price, 
                                        url: @training_program.url } },
          headers: @default_headers
    assert_redirected_to training_program_url(@training_program)
  end

  test "should fail to destroy training_program with enrollments" do
    assert_difference('TrainingProgram.count', 0) do
      delete training_program_url(@training_program), headers: @default_headers
    end

    # show edit with errors
    assert_response :success
  end
end
