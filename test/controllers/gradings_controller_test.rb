require 'test_helper'
require 'helpers/authentication_helper'

class GradingsControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper
  setup do
    @grading = gradings(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get gradings_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_grading_url, params: {grading: {person_id: people(:one).id}}, headers: @default_headers
    assert_response :success
  end

  test "should create grading" do
    assert_difference('Grading.count') do
      post gradings_url, 
           params: { grading: { grade_id: @grading.grade_id, 
                                notes: @grading.notes, 
                                person_id: @grading.person_id, 
                                started_at: @grading.started_at, 
                                count_start_date: @grading.count_start_date } },
           headers: @default_headers
    end

    assert_redirected_to grading_url(Grading.last)
  end

  test "should show grading" do
    get grading_url(@grading), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_grading_url(@grading), headers: @default_headers
    assert_response :success
  end

  test "should update grading" do
    patch grading_url(@grading), 
          params: { grading: { grade_id: @grading.grade_id, 
                               notes: @grading.notes, 
                               person_id: @grading.person_id, 
                               started_at: @grading.started_at } },
          headers: @default_headers
    assert_redirected_to grading_url(@grading)
  end

  test "should destroy grading" do
    assert_difference('Grading.count', -1) do
      delete grading_url(@grading), headers: @default_headers
    end

    assert_redirected_to gradings_url
  end
end
