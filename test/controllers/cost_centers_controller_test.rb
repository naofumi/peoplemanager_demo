require 'test_helper'
require 'helpers/authentication_helper'

class CostCentersControllerTest < ActionDispatch::IntegrationTest
  include AuthenticationHelper

  setup do
    @cost_center = cost_centers(:one)
    @default_headers = {}
    @default_headers['HTTP_AUTHORIZATION'] = basic_auth_header['HTTP_AUTHORIZATION']
  end

  test "should get index" do
    get cost_centers_url, headers: @default_headers
    assert_response :success
  end

  test "should get new" do
    get new_cost_center_url, headers: @default_headers
    assert_response :success
  end

  test "should create cost_center" do
    assert_difference('CostCenter.count') do
      post  cost_centers_url,
            headers: @default_headers,
            params: { cost_center: { description: @cost_center.description, number: 99999 } } # assuming 99999 has not been taken
    end

    assert_redirected_to cost_center_url(CostCenter.last)
  end

  test "should show cost_center" do
    get cost_center_url(@cost_center), headers: @default_headers
    assert_response :success
  end

  test "should get edit" do
    get edit_cost_center_url(@cost_center), headers: @default_headers
    assert_response :success
  end

  test "should update cost_center" do
    patch   cost_center_url(@cost_center),
            headers: @default_headers,
            params: { cost_center: { description: @cost_center.description, number: @cost_center.number } }
    assert_redirected_to cost_center_url(@cost_center)
  end

  test "should destroy cost_center" do
    cc = cost_centers(:orphan)
    assert_difference('CostCenter.count', -1) do
      delete cost_center_url(cc), headers: @default_headers
    end

    assert_redirected_to cost_centers_url
  end
end
