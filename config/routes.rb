Rails.application.routes.draw do
  resources :cost_centers
  resources :rafs
  resources :enrollments
  resources :training_categories
  resources :training_programs
  resources :gradings
  resources :grades do
    member do
      put 'up'
      put 'down'
    end
  end
  resources :groups do
    member do
      put 'up'
      put 'down'
    end
  end
  resources :job_statuses do
    member do
      put 'remove_person'
    end
  end
  resources :people
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  patch 'date', to: 'date#update'

  root to: 'groups#index'
end
