# Design Document

This document provides information on how the UI/UX should
be designed.

## Views

### Show views

#### Title
Use `h1` for headings that display significant information. For example,
use `h1` if you are displaying the Person's name in the Person#show header.

Otherwise, use `h3` for top-level headings. For example, if you have a title
such as "Edit Person", use `h3` since it doesn't have any significant information
and is just telling the user what is being done - no information about the
Person object is being displayed.

#### Displaying attributes

Show views are used to show the attributes of Objects. Since most
attributes are simple, we don't need much width to display the values.
Therefore, we use a 2-column layout for most of the attributes.
If an attribute contains a lot of information, then we can use a 1-column
layout for that.

```
<div class="row">
  <div class="col">
    <%= bs_attribute "Employee #" do %>
      <%= person.haken_at?(Time.zone.today) ? "派遣" : "" %>
      <%= person.employee_number %>
    <% end %>
  </div>
  <div class="col">
    <%= bs_attribute "e-mail", person.email %>
  </div>
</div>
<div class="row">
  <div class="col">
    <%= bs_attribute "Birth date", person.birth_date %>
  </div>
  <div class="col">
    <%= bs_attribute "Age", person.age %>
  </div>
</div>
```

#### Dividing attributes and associations

To separate the attributes of the primary object and its associations,
we will always use an `hr` tag. The `hr` tag should also be visible in
editing mode.

#### Controls

The purpose of a #show view is to show content and not to edit it. Therefore,
we should unsure that any controls in a #show view do not directly change
content, and at most, only link to pages/states where content can be changed.
Importantly, links to #destroy or #update should not be in a #show view.

A #show view will contain links to other pages. One special link is that which
opens up the Edit page for the primary Object. That is, if you are
on Person#show, then the link (or button) to Person#edit is a special primary edit
link.

This primary edit link should reside on the upper right corner of the page. It
should be displayed as a Bootstrap button.

If this #show page displays associated objects, then we will typically need
links to create new associated objects. For example, for Person#show, we will
be displaying Gradings and hence we will need a link to Grading#new. This
should be displayed as a small Bootstrap button (`.btn-sm`), on the upper right of the segment
where the Gradings objects are listed.

### Edit views

#### Titles

#### Forms and Fields

#### Controls

An #edit view will contain buttons like *Submit*, *Cancel* or *Destroy*.
*Submit* and *Cancel* should be displayed at the top-right of the page.
On the other hand, *Destroy* should be at the bottom-left of the page, after
all attributes have been displayed.

*Cancel* should be `.btn-secondary` and *Destroy* should be `btn-danger`.

*Cancel* buttons should always have a label that describes what is being
destructed. For example, instead of simply "destroy", it should read
"destroy person". Always try to be descriptive so that the user knows
what is going to be destroyed.

## User Experience

### REST

The default Rails scaffold has GET URLs for `new` and `edit`. Our idea is
to get rid of these. Neither `new` or `edit` are resources and are rather
awkward endpoints for GET. They are not part of REST either.
Also, you don't really want `new` or `edit` in the browser history.
Instead, our aim is to make the `new` and `edit` pages only accessible
through AJAX. I think that this will create a much better user experience.

`new` will be accessible from the #index page as a Modal dialog.

`edit` will be accessible from the #show page as Edit mode.

### Editing mode

To provide an experience with a minimum of transitions, Editing should be
done in an "editing mode". That is, instead of following a link to a
separate `edit` page, we should use JavaScript so that the `edit` page
is perceived as a different mode of the `show` page. The `edit` page
should be a different state of the `show` page and not something that is
separate.

The `edit` page will not have its own URL, and will not be added to the
browser history.

This will be achieved through the use of AJAX to replace the DOM of the
`show` page with an `edit` form.
