# People Manager TODO

## AJAX User Interface Design

### Objective

1. Use AJAX techniques to make it easy to input and edit data with ease
   and confidence for not techie users.


### Tools that we can use

1. Modal dialogs
   Modal dialogs allow you to show or edit information without leaving
   the current page. Since modal dialogs are smaller than the current viewport,
   modal dialogs should be used when the information displayed is brief.
   Also, scrolling inside a modal may have glitches since the browser will have
   to handle multiple scrollable windows (Bootstrap modals seem to be OK on
   the newest iOS). Therefore modal dialogs are more
   suitable for association tables like JobStatuses and Gradings.
   Modal dialogs are also useful if the Object is very simple.
2. Show-Edit replacements
   Show-Edit replacements allow you to be viewing information for an object,
   and then by clicking an `Edit` button, the page transforms to a page where
   you can edit the contents. This is very common in Smartphone interfaces with
   and `Edit` button often in the upper right corner.<br>
   By implementing this in AJAX, you no longer need a separate Edit page
   that would add an unnecessary entry into the Browser History. Therefore, the back-button experience is improved.
   Since

### Typical controller page flows

#### Modal dialog

Things to consider:<br>
For Modal dialogs, the server side will not know where the AJAX call originated
from. For example, a JobStatus#edit request may come from JobStatus#index or
a Raf#show or a People#show. Therefore, we send a reload after a successful
update.

1. When the `Edit` button is clicked, then send an AJAX call to retrieve the #edit
   page and insert that into the Modal dialog and display it.
2. When the #edit inside the Modal dialog is submitted as #update, if successful,
   then send a JavaScript response to reload the current page. Then the information
   will be updated to reflect the changes made. We send the flash notice with this.
3. If the #update is unsuccessful, then we resend the #edit contents to the Modal dialog.

#### Show-Edit replacement

1. When the `Edit` button is clicked, then send an AJAX call to retrieve the #edit
   page which will replace the innerHTML of the `#object-content` DOM element.
2. When the #edit is submitted and if successful, then a redirect to #show is done.
   This page will reflect the changes made. This works because a "Show-Edit replacement"
   will always come from a #show page.
3. When the #edit is unsuccessful, then the #edit is resent and replaces the innerHTML
   of the `#object-content` DOM element.

### Modal or Show-Edit replacements?

1. Modal dialogs
   * JobStatus
   * Gradings
2. Show-Edit replacements
   * People
     In addition to People information, this page will also show associated JobStatuses
     and Gradings.<br>
     From this page, you will be able to create a new JobStatus, indicating that the
     Person is taking a new job assignment. The JobStatus form should show up in
     a Modal.
     You should also be able to associate a pre-existing JobStatus to the Person
     that is currently being shown. Ensure that only JobStatuses that have no
     Person assignments yet, can be selected for the association. Otherwise, the
     Person previously assigned will be pushed out.
   * Group
     In addition to Group information, this page will also show a hierarchical
     view of the people belonging to this Group and below. It will also
     list all email addresses of the people belonging here.<br>
     There will also be a link that allows you to create a new JobStatus that
     links to the current Group. The link should open in a Modal dialog.
   * Grade
     In addition to the Grade information, this page will also show a list
     of all People who are in this grade.
   * RAF
     In addition to RAF information, this page will also list all the JobStatuses
     that are associated with this RAF. You will also be able to create a new
     JobStatus from the RAF page.

### Displaying the controls

In a smartphone user interface, the `Edit` or `Submit` controls are typically
displayed in a banner on the top of the page, or a control bar at the bottom.
This can be problematic when using Show-Edit replacements, since we would
expect the controls to change when switching between the show-edit modes, but
since these controls will be outside of the replaced fragment, it will be
tedious to replace them.

It looks like this --

In the `Show` view
```
<div id="controls">... some controls </div>
<div id="content">
  <div id="object-content">
      ... Show information about the person ...
  </div>
  <div id="associated-content">
    ... Content about associated objects ...
  </div>
</div>
<div

```

In the `Edit` view
```
<div id="controls">... some controls </div>
<div id="content">
  <div id="object-content">
      ... The editing fields is inserted into here via AJAX ...
  </div>
  <div id="associated-content">
    ... Content about associated objects ...
  </div>
</div>
<div

```
The problem in the `Edit` view is that it is tedious to change the controls.

### HTML Forms and controls

One characteristic of HTML forms is that you use a `form` tag to surround
all the elements of a form. Therefore, a form submit button will be inside
of the `form` tag. It is possible to work around this with JavaScript, but
the fact remains that this is not how HTML form work.

Therefore, putting form controls in a banner at the top of the page really
isn't how JavaScript forms work, and is something that we should avoid.

### Conclusion

1. When using a show-edit replacement scheme, the button that triggers this
   should not reside in a top-level control, such as in the top-page banner.
   Instead, the trigger button should be close to where the data is actually
   being shown, and should be part of what is being replaced.
2. On the other hand, if we are using a modal to edit content, the button
   can be in either location. If we are editing the top-level element
   (_i.e._ If we are on `Person#show` and wish to edit the `Person` object),
   then the trigger button should be in the top-page banner.
3. Given the above, we will probably use modal dialogs more, even when
   handling objects with lots of information and requiring scrolling. The reason
   for this is to ensure consistency in the UI and to place the `Edit` button
   always at the top-right.
